console.log("Installing PouchDB database...")

const PouchDB = require("pouchdb")
const fs = require('fs')

let db = new PouchDB("wiki")
  
fs.readFile('Accueil.md', 'utf8', function (err,data) {
    if (err) {
        return console.log(err)
    }
    db.post({ _id: "Accueil", content: data })
    .then(result => {
        console.log("Succesfully added home page")
    })
    .catch(error => {
        console.error("Error when adding home page")
        console.error(error)
    })

})
fs.readFile('API.md', 'utf8', function (err,data) {
    if (err) {
        return console.log(err)
    }
    db.post({ _id: "API", content: data, tags: ["Aide"] })
    .then(result => {
        console.log("Succesfully added API page")
    })
    .catch(error => {
        console.error("Error when adding API page")
        console.error(error)
    })
})



console.log("Finished post installation tasks")
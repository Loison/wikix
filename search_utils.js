const PouchDB = require("pouchdb")
PouchDB.plugin(require('pouchdb-find'))
let db = new PouchDB("wiki")

String.prototype.reverse = function() {
    return this.split('').reverse().join('')
}

var wholeWord = (length, start = 0) => {
    return new RegExp('^.{' + start + '}(.{' + length + '}[^\\s]*).*')
}

module.exports = {
    centerWord(text, word, options = {}) {
        const max_length = options.max_length || 150
        let regex = new RegExp('\\b' + word + '\\b', 'mgi'),
            match = regex.exec(text),
            position = match.index
        if(position + word.length < max_length) {
            return text.replace(wholeWord(max_length), '$1')
        } else if(position > text.length - max_length) {
            return text.reverse().replace(wholeWord(max_length), '$1').reverse()
        } else {
            let sidesLength = Math.floor((max_length - word.length) / 2),
                substrFromStart = text.reverse().replace(wholeWord(text.length - position + sidesLength), '$1')
                start = text.length - substrFromStart.length
            return text.replace(wholeWord(max_length, start), '$1')
        }
    },
    centerResults(results, regex) {
        let content = ''
        for(var i in results.docs) {
            content = results.docs[i].content
            let firstWord = content.match(regex)
            if(firstWord !== null) {
                firstWord = firstWord[0]
                results.docs[i].content = this.centerWord(content, firstWord)
            }
        }
        return results
    },
    getSelector(type, regex) {
        let regexFields
        switch (type) {
            case 'title':
                return { _id : { $regex: regex } }
                break
            case 'full':
            default:
                return {
                    $or: [
                        { _id : { $regex: regex } },
                        { content: { $regex: regex } }
                    ]
                }
        }
    },
    async performSearch(type, regex) {
        let selector = this.getSelector(type, regex)
        try {
            let res = await db.find({
                selector
            })
            return res
        } catch(error) {
            throw new Error(error)
        }
    },
    async getRandomDoc() {
        let allDocs = await db.allDocs({include_docs: false})
        let max = allDocs.rows.length
        let rand = Math.floor(Math.random() * max + 1)
        let result = await db.find({
            selector: {_id: {$type : 'string'}},
            limit : 1,
            skip : rand-1,
        })
        return result.docs[0]
    },
    async searchByTags(tags) {
        try {
            let res = await db.find({
                selector: {tags: {$in: tags}}
            })
            return res.docs
        } catch(error) {
            throw new Error(error)
        }
    }
}
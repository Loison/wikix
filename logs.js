const fs = require('fs')

module.exports = {
    access(text) {
        log('access', text)
    },
    error(text) {
        log('error', text)
    }
}

var log = (file, text) => {
    let finalText = new Date().toUTCString() +  '\t' + text
    fs.appendFile('./logs/' + file + '.log', finalText + '\n', (err) => {
        if(err) console.log('Error logging into access.log')
    })
}
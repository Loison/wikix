"use strict"

const Express = require("express")
const Url = require("url")
const BodyParser = require("body-parser")
const PouchDB = require("pouchdb")
const log = require("./logs")
const path = require('path')

const front_routes = Express.Router()

let db = new PouchDB("wiki")

var checkPath = (req, res, next) => {
    if (req._parsedUrl.pathname === "/") {
        res.redirect(req.baseUrl + "/Accueil")
        return
    }
    if (/%20/.test(req._parsedUrl.pathname)) {
        res.redirect(req.originalUrl.replace(/%20/g, "_"))
        return
    }

    req.pageName = req._parsedUrl.pathname.substr(1)
    next()
}

front_routes.use(checkPath)

front_routes.get("*", async (req, res) => {
    log.access("GET request for " + req.originalUrl)

    res.sendFile(path.join(__dirname + '/dist/index.html'))
})

module.exports = front_routes

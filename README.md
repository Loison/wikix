WikiX
=====

Wiki de Corentin et Louis

Setup
-----

Clone this repo

``` bash
git clone https://gitlab.com/Loison/wikix.git
```

Install the dependencies and setup the database

``` bash
npm install
```

Build production version

``` bash
npm run build
```

Usage
-----

Run the server

``` bash
node server.js
```

Go to `http://localhost:8080/`...

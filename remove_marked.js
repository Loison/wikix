const marked = require("marked")

var remover = new marked.Renderer()

remover.blockquote = (quote) => {
    return quote
}

remover.br = () => {
    return ' '
}

remover.code = (code, language, escaped) => {
    return code
}

remover.codespan = (code) => {
    return code
}

remover.del = (text) => {
    return text
}

remover.em = (text) => {
    return text
}

remover.heading = (text, level, rawtext) => {
    return rawtext
}

remover.hr = () => {
    return ''
}

remover.html = (text) => {
    return ''
}

remover.image = (href, title, text) => {
    return text
}

remover.link = (href, title, text) => {
    return text
}

remover.list = (body, ordered, start) => {
    return ' ' + body
}

remover.listitem = (text) => {
    return text + ' '
}

remover.paragraph = (text) => {
    return ' ' + text
}

remover.strong = (text) => {
    return text
}

remover.table = (header, body) => {
    return ''
}

remover.tablecell = (content, flags) => {
    return content + ' '
}

remover.tablerow = (content) => {
    return ''
}

module.exports = remover
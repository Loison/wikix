# Bienvenue sur WikiX

A deux doigts d'être aussi complet que Wikipédia, ce wiki partage ses valeurs :
- Ouverture : chacun peut modifier toutes les pages
- Connaissance : un amas de connaissances en perpétuelle évolution

</br>

## QFP (Questions Fréquemment Posées)

</br>

### Comment navigue-je dans cette magnifique interface ?

Chaque page est accessible via la recherche, ou via la page "Toutes les pages", mais aussi en recherchant par thème, dans la page dédiée.

### Comment modifie-je un article ?

Une fois sur un article, le bouton "Modifier" de la barre des tâches permet d'entrer en mode édition, avec tous les outils nécessaires à tout auteur de génie.

### Puis-je créer un nouvel article ?

Pour créer un nouvel article, il suffit de cliquer sur "Nouvelle page" dans la barre de navigation.

### Puis-je utiliser le wiki sans passer par l'interface ?

Bien-sûr ! En utilisant l'API à sur le port 8080 (infos [ici](/wiki/API))
const Express = require("express")
const API_routes = require("./API_routes")
const front_routes = require("./front_routes")
const cors = require("cors")

let app = Express()

app.use(cors())

app.use("/js", Express.static(__dirname + "/dist/js"))
app.use("/images", Express.static(__dirname + "/dist/images"))
app.use("/css", Express.static(__dirname + "/dist/css"))

app.use(Express.json())
app.use(Express.urlencoded())

app.use("/api", API_routes)
app.use("/wiki", front_routes)
app.get('/', (req, res) => {
    res.redirect('/wiki/Accueil')
})

let server = app.listen(8080, function() {
    let host = server.address().address
    let port = server.address().port

    console.log("Application listening at http://%s:%s", host, port)
})

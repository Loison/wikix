# Guide utilisateur de l'API

Adresse de base : `http://domain:8080/api/`

## Pages

Retourne le JSON d'une page du wiki 

* **URL**

  /pages

* **Methode :**

  `GET` | `POST` | `PUT` | `DELETE`
  
*  **Paramètres d'URL**

   `path=[string]` : selectionne une page par son id  
   OU  
   `query=[string]` : recherche dans toutes les pages selon le paramètre  
   OU  
   `tags=[array]` : recherche par tag  
   OU  
   `random` : retourne une page "au pif"

* **Paramètres en données**

  None

* **Réponse en cas de succès :**

  * **Code:** 200  
    **Contenu :**  
	+ Cas monopage (paramètres path et random) :
```
{
		"content": "# Bonjour",
		"tags": [
			"Cool",
			"Frais"
		],
		"_id": "Louis",
		"_rev": "9-0e7e3a53b25debedccd9590ce8bb0365"
}
```
	+ Cas multipage (paramètres query, tags ou pas de paramètres) :
```
{
		"total_rows": 8,
		"offset": 0,
		"rows": [
			{
				"content": "# Salut",
				"tags": [
					"Good",
					"Bien"
				],
				"_id": "Corentin",
				"_rev": "9-0e7e3a53b25debedccd9590ce8bb0365"
			},
			...
```

</br>

* **Réponse en cas d'erreur :**

  * **Code :** 404 NOT FOUND  
    **Contenu :** `missing`

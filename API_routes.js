"use strict"

const Express = require("express")
const Url = require("url")
const BodyParser = require("body-parser")
const PouchDB = require("pouchdb")
const log = require("./logs")
const searchUtils = require('./search_utils')
const EscapeRegexString = require('escape-string-regexp')

const API_routes = Express.Router()

let db = new PouchDB("wiki")

var checkPath = (req, res, next) => {
    if(Object.keys(req.query).length === 0 && req.method === 'GET') {
        req.type = 'ALLPAGES'
        next()
    } else if(typeof req.query.query !== 'undefined') {
        req.type = 'SEARCH'
        next()
    } else if (typeof req.query.tags !== "undefined") {
        req.type = 'TAGS'
        next()
    } else if (typeof req.query.random !== "undefined") {
        req.type = 'RANDOM'
        next()
    } else if (typeof req.query.path !== "undefined") {
        req.type = 'PAGE'
        req.pageName = req.query.path.replace(/_/g, ' ')
        next()
    } else {
        res.status(404).send("Unrecognized parameters")
    }
}

API_routes.use(checkPath)

API_routes.get("/pages", async (req, res) => {
    log.access("GET request for " + req.originalUrl)

    try {
        switch (req.type) {
            case 'PAGE':
                let doc = await db.get(req.pageName)
                res.json(doc)
                break
            case 'ALLPAGES':
                let docs = await db.allDocs({include_docs: true})
                res.json(docs)
                break
            case 'SEARCH':
                // check query parameters
                if(req.query.query === '') {
                    throw new Error('No search string.')
                }
                req.query.query = EscapeRegexString(req.query.query.replace(/\+/g, ' '))
                let type = req.query.type || 'full'
                type = type.toLowerCase()

                // perform db search
                let words, regex, results
                switch(type) {
                    case 'title':
                        words = req.query.query.replace(/ /g, '.+\\b'),
                        regex = new RegExp('.*\\b' + words + '.*', 'gi')
                        results = await searchUtils.performSearch(type, regex)
                        break
                    case 'full':
                    default:
                        words = req.query.query.replace(/ /g, '\\b.+\\b'),
                        regex = new RegExp('\\b(' + words + ')\\b', 'gi')
                        results = await searchUtils.performSearch(type, regex)
                        results = searchUtils.centerResults(results, regex)
                }
                
                res.json(results)
                break
            case 'TAGS':
                res.json(await searchUtils.searchByTags(req.query.tags.split(',')))
                break
            case 'RANDOM':
                res.json(await searchUtils.getRandomDoc())
                break
            default:
                throw new Error('Unknown request')
        }
    } catch(error) {
        log.error("Error accessing " + req.originalUrl + " : " + error)
        res.status(404).send(error.message)
    }
})

API_routes.post("/pages", async (req, res) => {
    log.access("POST request for " + req.originalUrl)

    try {
        if(typeof req.body.content === 'undefined') {
            throw {
                message: 'Missing POST parameter : content'
            }
        }

        await db.put({
            _id: req.pageName,
            content: req.body.content,
            tags: JSON.parse(req.body.tags) || null
        })

        res.send("Added " + req.pageName + " !")
    } catch (error) {
        log.error("Error adding " + req.pageName + " : " + error)
        res.status(404).send(error)
    }
})

API_routes.delete("/pages", async (req, res) => {
    log.access("DELETE request for " + req.originalUrl)
    
    try {
        if(req.pageName === 'Accueil') {
            throw new Error('Impossible de supprimer la page Accueil')
        }
        
        let doc = await db.get(req.pageName)
        db.remove(doc)

        res.send("Deleted " + req.pageName + " !")
    } catch (error) {
        log.error("Error deleting " + req.pageName + " : " + error)
        res.status(404).send(error)
    }
})

API_routes.put("/pages", async (req, res) => {
    log.access("PUT request for " + req.originalUrl)

    try {
        if(typeof req.body.content === 'undefined') {
            throw {
                message: 'Missing PUT parameter : content'
            }
        }
        let tags = req.body.tags || null
        let doc = await db.get(req.pageName)
        doc.content = req.body.content
        doc.tags = JSON.parse(tags)
        await db.put(doc)

        res.send("Updated " + req.pageName + " !")
    } catch (error) {
        log.error("Error updating " + req.pageName + " : " + error)
        res.status(404).send(error)
    }
})

module.exports = API_routes
